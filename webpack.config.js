const path = require("path");
const glob = require("glob");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackMd5Hash = require("webpack-md5-hash");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const PurgecssPlugin = require("purgecss-webpack-plugin");
const isDevServer = process.argv.find(v => v.indexOf("webpack-dev-server") !== -1);
conf = {
  entry: { main: "./src/index.tsx" },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].[chunkhash].js",
    pathinfo: false
  },
  devtool: "inline-source-map",
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /node_modules/,
          chunks: "all",
          name: "vendor",
          enforce: true
        },
        styles: {
          name: "styles",
          test: /\.css$/,
          chunks: "all",
          enforce: true
        },
        default: {
          minChunks: 2,
          reuseExistingChunk: true
        }
      }
    }
  },
  devServer: {
    overlay: true
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        loader: "babel-loader!ts-loader",
        exclude: /node_modules/
      },
      // {
      //   test: /\.js(x?)$/,
      //   loader: "babel-loader",
      //   exclude: /node_modules/
      // },
      {
        test: /\.(s?)css$/,
        use: ["style-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "sass-loader"]
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style.[contenthash].css"
    }),
    new CopyWebpackPlugin([{ from: "./src/assets", to: "./assets" }]),
    new WebpackMd5Hash()
  ]
};

module.exports = (env, options) => {
  const production = options.mode === "production";
  conf.devtool = production ? false : "eval-sourcemap";
  conf.plugins = [
    ...conf.plugins,
    new HtmlWebpackPlugin({
      hash: true,
      compile: false,
      inject: true,
      template: production && !isDevServer ? "!!prerender-loader?string!src/index.html" : "src/index.html",
      filename: "index.html"
    })
  ];
  if (production)
    conf.plugins = [
      new CleanWebpackPlugin(["dist/*.*"], {}),
      new PurgecssPlugin({
        paths: glob.sync(path.join(__dirname, "src/**/*"), {
          nodir: true
        }),
        whitelistPatterns: [
          /md:max-w-lg/,
          /md:w-1\/5/,
          /w-1\/3/,
          /md:w-3\/5/,
          /w-3\/4/,
          /sm:w-1\/2/,
          /sm:rounded-l-none/,
          /md:text-4xl/,
          /md:text-base/,
          /md:pl-8/
        ]
      }),
      ...conf.plugins
    ];
  return conf;
};
