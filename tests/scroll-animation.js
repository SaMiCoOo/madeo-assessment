const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto("file:///home/tayel/lab/test/dist/index.html");

  const scrollTests = await testScrollAnimation(page);
  const scrollAnimationPassed = scrollTests.map(test => test.passed).reduce((a, b) => a && b, true);
  console.log({ scrollAnimationPassed });
  browser.close();
})();
async function testScrollAnimation(page) {
  let tests = [];
  const res = await page.evaluate(async tests => {
    const testPatternHeight = () => {
      const rect = document.getElementsByTagName("rect")[0];
      const patternId = rect.__preactattr_.fill.split("#")[1].split(")")[0];
      const pattern = document.getElementById(patternId);
      const height = pattern.__preactattr_.height;
      return { passed: patternId == height, patternId, height };
    };
    const distance = 100;
    let totalHeight = 0;
    const promise = new Promise(resolve => {
      const timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        tests.push(testPatternHeight());
        totalHeight += distance;
        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve(tests);
        }
      }, 1000 / 60);
    });

    return await promise;
  }, tests);
  return res;
}

testPatternHeight = async page => {
  return await page.evaluate(() => {
    const rect = document.getElementsByTagName("rect")[0];
    const patternId = rect.__preactattr_.fill.split("#")[1].split(")")[0];
    const pattern = document.getElementById(patternId);
    const height = pattern.__preactattr_.height;
    console.log("wwoo");
    return { passed: patternId == height, patternId, height };
  });
};
