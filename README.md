check live demo here https://madeo-assessment.firebaseapp.com/

### To run locally

simply clone and run

```bash
npm start
```

### To build and load static html file

build using

```bash
npm run build
```

Then go to the dist directory and open `index.html` in your browser.

### To Run tests

first install test depenedencies using

```bash
npm run test:install
```

then run:

```bash
npm run test
```

This will build the project then load it into a headless chrome browser and test the scrolling animation inside of it
