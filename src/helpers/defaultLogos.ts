export const defaultLogos: ImageDetail[] = [
  "android",
  "chrome-dev",
  "dailmer",
  "facebook",
  "firefox",
  "google",
  "macos",
  "mercedes",
  "ng-conf",
  "reddit",
  "twitter",
  "youtube"
].map(image => {
  const details = {
    src: `./assets/logos/${image}.png`,
    alt: image
  };
  return details;
});
export interface ImageDetail {
  src: string;
  alt: string;
}
