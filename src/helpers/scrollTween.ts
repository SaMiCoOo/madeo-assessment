/**
 * Tween an object due to it's position to the current viewport scroll position
 * with a virtual offset from the viewport top border
 *
 * @param {Element} element Element to be animated
 * @param {number} offset virtual offset from viewport top border
 * @returns {number} value between 0 to 1
 */
export const scrollTweenElementWithOffset = (
  element: Element,
  offset: number = 0
): number => {
  // element distance to screen top bar
  const elementTop = element.getBoundingClientRect().top;
  const elementBottom = element.getBoundingClientRect().bottom;
  const elementHeight = element.clientHeight;
  // distance between `Element Top Border` and virtual offset from `viewport top border` in px
  const distanceToOffset = elementTop - offset;

  /*
    Emit Tween value as long as the virtual offset is intersecting with the 
    element and if the element is still visible but is not intersecting anymore
    emit `1` to keep the effect at it's max value

    Ensure tween emits value as long as the element is visible on the screen
    Which is true as long a the distance from element top border to viewport
    top border `elementTop` is less than the viewport height
    OR distance element bottom border to viewport top border has a positive value
  */

  if (elementTop < window.innerHeight || elementBottom > 0) {
    if (distanceToOffset * -1 >= elementHeight) {
      return 1;
    }
    if (distanceToOffset < 0) {
      return (distanceToOffset * -1) / elementHeight;
    }
    return 0;
  }
  return 0;
};

/**
 * Tween an object due to it's position to the viewport top border
 *
 * @param {Element} element Element to be animated
 * @returns
 */
export const scrollTweenElementWithViewportTop = (element: Element) => {
  return scrollTweenElementWithOffset(element);
};

/**
 * Tween an object due to it's position to the viewport bottom border
 *
 * @param {Element} element Element to be animated
 * @returns {number} value between 0 to 1
 */
export const scrollTweenElementWithViewportBottom = (element: Element) => {
  const offset = window.innerHeight;
  return scrollTweenElementWithOffset(element, offset);
};
