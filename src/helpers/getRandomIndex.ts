export const getRandomIndex = (array: any[]) => {
  return Math.floor(Math.random() * array.length);
};
