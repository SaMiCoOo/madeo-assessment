import "./css/style.scss";
import { h, render } from "preact";
import { App } from "./components/app";
if (document.getElementsByTagName("main").length === 0) {
  render(<App />, document.body);
} else {
  render(<App />, document.body, document.getElementsByTagName("main")[0]);
}
