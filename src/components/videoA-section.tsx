import { FunctionalComponent, h } from "preact";
import { SectionTitle } from "./title";
import { DotPattern } from "./dot-pattern";
import { DotPatternDemo } from "./dot-pattern-demo";

let offset = 200;
export const VideoASection: FunctionalComponent = ({ ...props }) => (
  <div>
    <SectionTitle title="Video A: Scrolling Tweened Animation" subtitle="I am bad at naming things 😄" />
    <blockquote className="border-l-4 border-teal italic my-8 pl-4 md:pl-8">
      Interaction can be found on a video from the link below:
      <br />
      <a href="https://drive.google.com/file/d/1MSNZ9EkY79DvEuwMHFALULdhr7QvKcw1/view?usp=sharing">
        https://drive.google.com/file/d/1MSNZ9EkY79DvEuwMHFALULdhr7QvKcw1/view?usp=sharing
      </a>
      <br />
      Please note that dots scrolling interaction is to be managed through browser scrollbar, where as the scroll goes
      down dots get closer sequentially, and in correlation with scrollbar movement (not after scrollbar moves and
      stops, but while the scrollbar is moving), then points needs to get away sequentially on scrolling up with the
      same correlation to scrollbar movement.
      <br />
      Please note that the dots should be in a fluid div/section on a page not taking a full page height, where there is
      an empty section with minimum height of 600px followed by the div/section where it has the dots animation and with
      height of 450px, followed by an empty space of 600px.
    </blockquote>
    <h4>Requirements</h4>
    <ol>
      <li className="py-3">Scroll Tweened (have in between keyframes based on state) Animation</li>
      <li className="py-3">Animated Element should be a fluid</li>
      <li className="py-3">
        Element should be of minimum <b>450px</b> height preceeded and followed by <b>600px</b> height divs
      </li>
    </ol>
    <h4>Notes</h4>
    <p className="py-6">
      There was no clear statement what scroll position should be used to animate the element, when it collide with the
      bottom border of the viewport, or when it collides the top border of the viewport, so I made a component able to
      detect collision based on a dynamic offset value, you can test it below
    </p>
    <DotPatternDemo />
  </div>
);
