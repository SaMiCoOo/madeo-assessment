import { h, Component } from "preact";
import { DotPattern } from "./dot-pattern";
import { SwappingImages } from "./swapping-images";
import { defaultLogos } from "../helpers/defaultLogos";

export class SwappingImagesDemo extends Component {
  render() {
    return (
      <div className=" rounded-lg overflow-hidden shadow-lg bg-grey-lightest">
        <h4 className="font-bold text-xl mb-4 pl-6 pt-6">Demo</h4>
        <SwappingImages images={defaultLogos} />
      </div>
    );
  }
}
