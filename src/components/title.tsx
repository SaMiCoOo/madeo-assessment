import { h, FunctionalComponent } from "preact";

export const PageTitle: FunctionalComponent<TitleProps> = ({
  title,
  subtitle,
  ...props
}) => (
  <div {...props}>
    <h1 className="font-sans break-normal text-black pt-6 pb-2 text-3xl md:text-4xl">
      {title}
    </h1>
    {subtitle && (
      <p className="text-sm md:text-base font-normal text-grey-dark">
        {subtitle}
      </p>
    )}
  </div>
);

export const Paragraph: FunctionalComponent = ({ children, ...props }) => (
  <p className="py-6" {...props}>
    {children}
  </p>
);

export const SectionTitle: FunctionalComponent<TitleProps> = ({
  title,
  subtitle,
  children,
  ...props
}) => (
  <div {...props}>
    <h3 className="break-normal text-black pt-6 pb-2 text-1xl md:text-2xl">
      {title}
    </h3>
    {subtitle && (
      <p className="text-sm md:text-base font-normal text-grey-dark">
        {subtitle}
      </p>
    )}
  </div>
);

interface TitleProps {
  title: string;
  subtitle?: string;
}
