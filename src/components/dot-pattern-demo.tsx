import { h, Component } from "preact";
import { DotPattern } from "./dot-pattern";
export class DotPatternDemo extends Component<{}, DotPatternDemoState> {
  toggleLine() {
    this.setState({ hidden: !this.state.hidden });
  }
  handleChange(event: any) {
    const offset = parseInt(event.target.value, 10);
    this.setState({ offset: isNaN(offset) ? 0 : offset });
    console.log(this.state.offset);
  }

  componentWillMount() {
    this.setState({ hidden: true, offset: 0, hovering: false });
  }

  render() {
    return (
      <div className=" rounded-lg overflow-hidden shadow-lg bg-grey-lightest">
        <h4 className="font-bold text-xl mb-4 pl-6 pt-6">Demo</h4>

        <div
          className={
            "h-1 bg-red w-full fixed opacity-25" +
            (!this.state.hidden || (this.state.hovering && this.state.hidden) ? "" : " hidden")
          }
          style={{ left: 0, top: this.state.offset }}
        />
        <p className="py-2 text-grey-darkest text-center px-2">
          As it collides with a virtual line <b>{this.state.offset}px</b> from the viewport top border
        </p>
        <p className="py-2 text-sm text-teal-dark text-center px-2">
          you can change the offset and toggle the line visibility using the following inputs
        </p>
        <div className="flex justify-around sm:flex-row flex-col items-center">
          <div className="flex flex-wrap items-stretch w-3/4 md:w-3/5 relative">
            <input
              onInput={this.handleChange.bind(this)}
              type="text"
              className="flex-auto leading-normal w-32 flex-1 mb-2 border h-12 border-grey-light rounded sm:rounded-r-none px-3 relative bg-white text-black"
              placeholder="0"
            />
            <span className="flex flex-auto sm:-ml-px mb-2 items-center leading-normal bg-grey-light min-h-12 rounded sm:rounded-l-none border border-grey-light px-3 text-grey-darkest text-sm">
              px from the viewport top port
            </span>
          </div>
        </div>
        <div className="flex justify-center mt-4">
          <button
            onClick={this.toggleLine.bind(this)}
            onMouseEnter={() => this.setState({ hovering: true })}
            onMouseLeave={() => this.setState({ hovering: false })}
            className={
              "p-4 px-8 w-3/4 md:w-3/5 rounded border-1 hover:bg-red-lighter text-black border-grey border focus:outline-none" +
              (this.state.hidden ? "" : " bg-red-lighter")
            }
          >
            Toggle Line Visibility
          </button>
        </div>
        <div className="bg-orange mt-32">
          <DotPattern offset={this.state.offset}>
            <div className="py-32 flex items-center flex-wrap">
              <h2>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ea corporis nostrum suscipit repudiandae
                nisi omnis qui aspernatur error! Aliquam eos, numquam nisi harum eligendi facilis voluptatum unde veniam
                dolor?
              </h2>
              <h2>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ea corporis nostrum suscipit repudiandae
                nisi omnis qui aspernatur error! Aliquam eos, numquam nisi harum eligendi facilis voluptatum unde veniam
                dolor?
              </h2>
              <h2>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ea corporis nostrum suscipit repudiandae
                nisi omnis qui aspernatur error! Aliquam eos, numquam nisi harum eligendi facilis voluptatum unde veniam
                dolor?
              </h2>
              <h2>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ea corporis nostrum suscipit repudiandae
                nisi omnis qui aspernatur error! Aliquam eos, numquam nisi harum eligendi facilis voluptatum unde veniam
                dolor?
              </h2>

              <h2>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ea corporis nostrum suscipit repudiandae
                nisi omnis qui aspernatur error! Aliquam eos, numquam nisi harum eligendi facilis voluptatum unde veniam
                dolor?
              </h2>
              <h2>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ea corporis nostrum suscipit repudiandae
                nisi omnis qui aspernatur error! Aliquam eos, numquam nisi harum eligendi facilis voluptatum unde veniam
                dolor?
              </h2>
            </div>
          </DotPattern>
        </div>
      </div>
    );
  }
}

interface DotPatternDemoState {
  offset: number;
  hidden: boolean;
  hovering: boolean;
}
