import { h, Component } from "preact";
import { Subscription, fromEvent } from "rxjs";
import { scrollTweenElementWithOffset } from "../helpers/scrollTween";
import { throttleTime } from "rxjs/operators";

/**
 * A component that displays an svg background that is animated by scrolling over it
 *
 * Takes only 1 property `offset` which determines the offset from the viewport top border
 * that the animation will start when the component top border start colliding with
 *
 *
 * @export
 * @class DotPattern
 * @extends {Component<DotPatternProps, DotPatternState>}
 */
export class DotPattern extends Component<DotPatternProps, DotPatternState> {
  offset: number = 0;
  element: Element | null = null;
  scrollEventSubscription: Subscription | null = null;

  componentWillMount() {
    this.setState({ height: 80 });
  }

  /**
   * Subscribes to an observable to the document scroll event
   * that is throttled to 60 events / second, and animates the background
   * through changing the pattern height
   *
   *
   * @returns
   * @memberof DotPattern
   */
  resubscribeAnimator() {
    if (this.scrollEventSubscription) {
      this.scrollEventSubscription.unsubscribe();
    }
    return (
      fromEvent(document, "scroll")
        // 60 fps limit
        .pipe(throttleTime(1000 / 60))
        .subscribe(() => {
          // recalculate height of pattern
          if (this.element !== null) {
            const height = 80 - 60 * scrollTweenElementWithOffset(this.element, this.offset);
            this.setState({ height });
          }
        })
    );
  }

  componentDidUpdate() {
    // other than setting the offset with a number it can be set with `top` | `bottom`
    // top = 0, bottom = window.innerheight
    if (typeof this.props.offset === "string") {
      this.offset = this.props.offset === "bottom" ? window.innerHeight : 0;
    } else {
      this.offset = this.props.offset;
    }
  }
  componentDidMount() {
    this.scrollEventSubscription = this.resubscribeAnimator();
  }

  componentWillUnmount() {
    if (this.scrollEventSubscription !== null) {
      this.scrollEventSubscription.unsubscribe();
    }
  }

  render() {
    return (
      <div
        ref={rect => {
          this.element = rect;
        }}
        className="h-full w-full relative"
      >
        <svg className="absolute z-0 block" height="100%" width="100%">
          <rect className="z-0 block" width="100%" height="100%" fill={`url(#${this.state.height})`} opacity="0.5" />

          <defs>
            <pattern
              className="z-0"
              id={this.state.height.toString()}
              x="25"
              y="30"
              width="20"
              height={this.state.height}
              patternUnits="userSpaceOnUse"
            >
              <circle cx="10" cy="3" r="3" className="z-0 block" stroke="none" fill="#e3d49b" />
            </pattern>
          </defs>
        </svg>
        <div className="w-full h-full z-50">{this.props.children}</div>
      </div>
    );
  }
}

interface DotPatternProps {
  offset: "top" | "bottom" | number;
}

interface DotPatternState {
  height: number;
}
