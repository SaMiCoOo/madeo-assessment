import { h, FunctionalComponent } from "preact";
import { SwappingImagesDemo } from "./swapping-images-demo";
export const VideoBSection: FunctionalComponent = ({ ...props }) => (
  <div>
    <h3>Video B: Fading in and out logos</h3>
    <blockquote className="border-l-4 border-teal italic my-8 pl-8 md:pl-12">
      Interaction can be found on a video from the link below:
      <br />
      <a href="https://drive.google.com/file/d/1ipifO6WUxkz1mr-9KtTpjfjk_IsET-p2/view?usp=sharing">
        https://drive.google.com/file/d/1ipifO6WUxkz1mr-9KtTpjfjk_IsET-p2/view?usp=sharing
      </a>
      <br />
      The intended animation is to randomly display different logos across a 4 columns grid, please note the following:
      <br />
      <ol>
        <li>Create an array of 12 logos</li>
        <li>A logo shouldn’t appear twice on the screen</li>
        <li>Column where logo change should be random (i.e first, third, second, first, last, ..etc.)</li>
        <li>You are free to use any content for rotating images.</li>
      </ol>
    </blockquote>
    <SwappingImagesDemo />
  </div>
);
