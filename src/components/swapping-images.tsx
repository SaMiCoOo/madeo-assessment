import { h, Component } from "preact";
import { interval, Subscription } from "rxjs";
import { delay, map } from "rxjs/operators";
import { getRandomIndex } from "../helpers/getRandomIndex";

/**
 * A component that takes an array of minimum 5 `images` and displays
 * randomly 4 of them across a grid of 4 columns, and makes sure none of
 * the images are displayed twice on the screen at the same time.
 *
 * @export
 * @class SwappingImages
 * @extends {Component<SwappingImagesProps, SwappingImagesState>}
 */
export class SwappingImages extends Component<
  SwappingImagesProps,
  SwappingImagesState
> {
  displayedImages: ImageDetail[];
  hiddenImages: ImageDetail[];

  /**
   * Used to keep track of the image swapping animation subscription
   * and unsubscribe it on componentWillUnMount hook
   *
   * @type {Subscription}
   * @memberof SwappingImages
   */
  animationSubscription: Subscription;
  constructor({ ...props }) {
    super();
    // Split the image into 4 to be displayed while else are hidden
    this.displayedImages = props.images;
    this.hiddenImages = this.displayedImages.splice(4);
    this.animationSubscription = this.startAnimation();
  }

  componentWillUnmount() {
    this.animationSubscription.unsubscribe();
  }

  /**
   * Starts the image swapping animation
   *
   * @returns {Subscription} Subscription to the animation interval should be unsubscribed on unLoad
   * @memberof SwappingImages
   */
  startAnimation(): Subscription {
    return interval(1000)
      .pipe(
        map(() => {
          const toBeIntroducedIndex = getRandomIndex(this.hiddenImages);
          const toBeRemovedIndex = getRandomIndex(this.displayedImages);
          this.setState({ swappingIdx: toBeRemovedIndex });
          return { toBeIntroducedIndex, toBeRemovedIndex };
        }),
        delay(500),
        map(({ toBeIntroducedIndex, toBeRemovedIndex }) => {
          // Magic of es6, swap in one line or not ...
          [
            this.displayedImages[toBeRemovedIndex],
            this.hiddenImages[toBeIntroducedIndex]
          ] = [
            this.hiddenImages[toBeIntroducedIndex],
            this.displayedImages[toBeRemovedIndex]
          ];
          this.setState({
            swappingIdx: 5
          });
          return;
        })
      )
      .subscribe();
  }
  render() {
    return (
      <div className="flex flex-wrap my-8 justify-around">
        {this.displayedImages.map((image, idx) => (
          <div className="flex items-center min-h-64 md:w-1/5 w-1/3 mx-4 my-2  img-container">
            <img
              className={
                this.state.swappingIdx === idx
                  ? "swing-out-n-fade"
                  : "swing-n-fade"
              }
              src={image.src}
              alt={image.alt}
            />
          </div>
        ))}
      </div>
    );
  }
}

interface SwappingImagesProps {
  images: ImageDetail[];
}

interface SwappingImagesState {
  swappingIdx: number;
}

export interface ImageDetail {
  src: string;
  alt: string;
}
